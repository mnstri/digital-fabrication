---
title: "02 | Computer-Aided Design"
draft: true
categories: ['Weekly Assignments']
summary: 'Summary of the post'
---

## Weekly assignments

{{< hint info >}}
**Computer-Aided Design**  
1. Model (raster, vector, 2D, 3D, render, animate, simulate, ...) a possible final project, compress your images and videos, and post it on your class page.

**Have you?**  
- [ ] Modelled experimental objects/part of a possible project in 2D and 3D software.
- [ ] Shown how you did it with words/images/screenshots.
- [ ] Included your original design files.
{{< /hint >}}