# Matti's Digital Fabrication

My Digital Fabrication website is here:

[mnstri.gitlab.io/digital-fabrication/](https://mnstri.gitlab.io/digital-fabrication/)

I will try to keep this as the same as my official Fab Academy website so the Aalto students can also see what I do.
