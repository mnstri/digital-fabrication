---
title: "03 | Computer-Controlled Cutting"
draft: true
categories: ['Weekly Assignments']
summary: 'Summary of the post'
---

## Weekly assignments

{{< hint info >}}
**Group Assignment**  
1. Characterize your lasercutter's focus, power, speed, rate, kerf, and joint clearance.
2. Document your work (individually or in group).

**Individual Assignments**
1. Design, lasercut, and document a parametric press-fit construction kit, which can be assembled in multiple ways. Account for the lasercutter kerf.
2. Cut something on the vinylcutter.


**Have you?**  

- [ ] linked to the group assignment page
- [ ] Explained how you parametrically designed your files
- [ ] Documented how you made your press-fit kit
- [ ] Documented how you made your vinyl cutting
- [ ] Included your original design files
- [ ] Included your hero shots

{{< /hint >}}