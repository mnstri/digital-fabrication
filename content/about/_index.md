---
title: "About"
date: 2022-01-06T12:34:31+02:00
draft: false
weight: 10
image: "matti_kolvi.jpg"
featured: true
---

# Matti Niinimäki

Hi! I'm Matti. I am a Senior University Lecturer at Aalto University and the Head of the New Media major at the Department of Art and Media.

- 🕸️ [My website](https://mansteri.com/)
- 📷 [Instagram](https://www.instagram.com/matti_niinimaki/)
- 🐤 [Twitter](https://twitter.com/mnstri)
- 🖇️ [LinkedIn](https://www.linkedin.com/in/mattiniinimaki/)

## Motivation

I had a couple of reasons to join Fab Academy:

1. I coordinate the Digital Fabrication courses together with [Krisjanis](https://fabacademy.org/2018/labs/barcelona/students/krisjanis-rijnieks/final-project/) and [Solomon](https://fabacademy.org/2019/labs/aalto/students/solomon-embafrash/index.html) here at Aalto University. These courses are the way for Fab Academy students to include the program to their studies at the university, so I have been following the last two years of Fab Academy quite closely. We are attempting to further develop the integration of Fab Academy to our MA studies so it seemed to make sense for me to also officially complete it.
2. I am using this opportunity to develop some teaching materials for the other courses I teach here (Introduction to Making and Physical Computing).
3. The most important reaason for me is to just dedicate some time to actually make something that I want. Recent years, I have been mainly working on commissions for museums and teaching a lot. The moments when I have time to just work on my own artworks or other projects have been quite rare.

## What have I done before?