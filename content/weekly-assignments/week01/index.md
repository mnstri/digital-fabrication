---
title: '01 | Principles and Practices + Project Management'
draft: false
image: img/featured.jpg
description: 'Week 1'
summary: 'This is a summary of the post'
type: 'posts'
categories: ['Weekly Assignments']
tags: ['project management', 'version control','hugo', 'git']
asciinema: true
type: 'weekly-assignments'
---

## Weekly assignments

{{< hint checklist >}}
**Principles and Practices**  
1. Plan and sketch a potential final project.

**Have you?**  
- [x] Read the [Fab Charter](http://fab.cba.mit.edu/about/charter/)
- [ ] Sketched your final project idea/s
- [ ] Described what it will do and who will use it

{{< /hint >}}

{{< hint checklist >}}
**Project Management**  
1. Work through a git tutorial.  
2. Build a personal site in the class archive describing you and your final project.

**Have you?**  
- [ ] Made a website and described how you did it
- [ ] Introduced yourself
- [ ] Documented steps for uploading files to archive
- [x] Pushed to the class archive
- [x] Signed and uploaded Student Agreement

{{< /hint >}}